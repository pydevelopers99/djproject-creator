#!/bin/bash

read -p "What's your name : " name
echo "Hi $name,\n\t One thing you can never recycle is wasted TIME."
echo "So let's start ...!"

read -p "Directory Name : " dirname
mkdir $dirname
echo "Directory created successfully." 

read -p "Virtualenv Name : " env_name
cd $dirname && virtualenv $env_name 
echo "Installing django ..........."
$env_name/bin/pip install django  

echo "Virtualenv has been created with Django."

read -p "Project Name : " pro_name
$env_name/bin/django-admin startproject $pro_name
echo $pro_name "has been created successfully"

read -p "Enter number of package : " max_no_pkg
for i in $(seq 1 $max_no_pkg);
do
    read -p "Enter package name you want to install : " pkg
    $env_name/bin/pip install $pkg
    echo $pkg "has been Installed successfully..."
done

read -p "Enter number of apps : " max_no_app
for i in $(seq 1 $max_no_app);
do
    read -p "Application name [ $i.] : " app_name
    cd $pro_name && ../$env_name/bin/python manage.py startapp $app_name 
    echo $app_name "has been created sucessfully."
    cd ..
done

exec bash